(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["user-list-user-list-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/user-list/user-list.page.html":
/*!*************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/user-list/user-list.page.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content class=\"ion-padding\">\n  <ng-container *ngIf=\"userInfo.length; else name\">\n    <ion-grid fixed>\n      <ion-virtual-scroll [items]=\"userInfo\" approxItemHeight=\"290px\">\n        <ion-row\n          class=\"user-content ion-align-items-center ion-activatable\"\n          *virtualItem=\"let item; let i = index\"\n          (click)=\"goToUserDetail(item, i)\"\n        >\n          <ion-col size=\"auto\">\n            <ion-avatar class=\"user-content__avatar\">\n              <ion-img [src]=\"item.picture.medium\"></ion-img>\n            </ion-avatar>\n          </ion-col>\n          <ion-col>\n            <ion-row>\n              <ion-col size=\"12\">\n                <h3 class=\"user-content__first-name\">\n                  {{item.name.first}} {{item.name.last}}\n                </h3>\n              </ion-col>\n              <ion-col size=\"12\">\n                <h4 class=\"user-content__last-name\">\n                  {{item.location.country}}\n                </h4>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n          <ion-ripple-effect></ion-ripple-effect>\n        </ion-row>\n      </ion-virtual-scroll>\n    </ion-grid>\n\n    <ion-infinite-scroll\n      threshold=\"100px\"\n      (ionInfinite)=\"loadMoreUsersByScroll($event)\"\n    >\n      <ion-infinite-scroll-content\n        loadingSpinner=\"bubbles\"\n        loadingText=\"Loading more data...\"\n      >\n      </ion-infinite-scroll-content>\n    </ion-infinite-scroll>\n  </ng-container>\n\n  <ng-template #name>\n    <ion-row class=\"spinner-container\" align-items-center justify-content-center>\n      <ion-spinner></ion-spinner>\n    </ion-row>\n  </ng-template>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/user-list/user-list-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/user-list/user-list-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: UserListPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserListPageRoutingModule", function() { return UserListPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _user_list_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./user-list.page */ "./src/app/user-list/user-list.page.ts");




const routes = [
    {
        path: '',
        component: _user_list_page__WEBPACK_IMPORTED_MODULE_3__["UserListPage"]
    }
];
let UserListPageRoutingModule = class UserListPageRoutingModule {
};
UserListPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], UserListPageRoutingModule);



/***/ }),

/***/ "./src/app/user-list/user-list.module.ts":
/*!***********************************************!*\
  !*** ./src/app/user-list/user-list.module.ts ***!
  \***********************************************/
/*! exports provided: UserListPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserListPageModule", function() { return UserListPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _user_list_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./user-list-routing.module */ "./src/app/user-list/user-list-routing.module.ts");
/* harmony import */ var _user_list_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./user-list.page */ "./src/app/user-list/user-list.page.ts");







let UserListPageModule = class UserListPageModule {
};
UserListPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _user_list_routing_module__WEBPACK_IMPORTED_MODULE_5__["UserListPageRoutingModule"]
        ],
        declarations: [_user_list_page__WEBPACK_IMPORTED_MODULE_6__["UserListPage"]]
    })
], UserListPageModule);



/***/ }),

/***/ "./src/app/user-list/user-list.page.scss":
/*!***********************************************!*\
  !*** ./src/app/user-list/user-list.page.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".user-content {\n  margin-bottom: 5px;\n}\n.user-content__avatar {\n  height: 60px;\n  width: 60px;\n  margin-right: 10px;\n}\n.user-content__first-name {\n  font-size: 1rem;\n  font-weight: bold;\n  margin: 0;\n}\n.user-content__last-name {\n  font-size: 0.85rem;\n  margin: -5px 0 0;\n}\n.spinner-container {\n  height: 100vh;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9qb2huZnVlbnRlcy9Eb2N1bWVudHMvUHJvamVjdHMvaW9uaWMgL2x1aXMgcGFsYWNpb3Mvc3JjL2FwcC91c2VyLWxpc3QvdXNlci1saXN0LnBhZ2Uuc2NzcyIsInNyYy9hcHAvdXNlci1saXN0L3VzZXItbGlzdC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxrQkFBQTtBQ0NGO0FEQUU7RUFDRSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0FDRUo7QURBRTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFNBQUE7QUNFSjtBREFFO0VBQ0Usa0JBQUE7RUFDQSxnQkFBQTtBQ0VKO0FEQ0E7RUFDRSxhQUFBO0FDRUYiLCJmaWxlIjoic3JjL2FwcC91c2VyLWxpc3QvdXNlci1saXN0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi51c2VyLWNvbnRlbnQge1xuICBtYXJnaW4tYm90dG9tOiA1cHg7XG4gICZfX2F2YXRhciB7XG4gICAgaGVpZ2h0OiA2MHB4O1xuICAgIHdpZHRoOiA2MHB4O1xuICAgIG1hcmdpbi1yaWdodDogMTBweDtcbiAgfVxuICAmX19maXJzdC1uYW1lIHtcbiAgICBmb250LXNpemU6IDFyZW07XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgbWFyZ2luOiAwO1xuICB9XG4gICZfX2xhc3QtbmFtZSB7XG4gICAgZm9udC1zaXplOiAwLjg1cmVtO1xuICAgIG1hcmdpbjogLTVweCAwIDA7XG4gIH1cbn1cbi5zcGlubmVyLWNvbnRhaW5lciB7XG4gIGhlaWdodDogMTAwdmg7XG59XG4iLCIudXNlci1jb250ZW50IHtcbiAgbWFyZ2luLWJvdHRvbTogNXB4O1xufVxuLnVzZXItY29udGVudF9fYXZhdGFyIHtcbiAgaGVpZ2h0OiA2MHB4O1xuICB3aWR0aDogNjBweDtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xufVxuLnVzZXItY29udGVudF9fZmlyc3QtbmFtZSB7XG4gIGZvbnQtc2l6ZTogMXJlbTtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIG1hcmdpbjogMDtcbn1cbi51c2VyLWNvbnRlbnRfX2xhc3QtbmFtZSB7XG4gIGZvbnQtc2l6ZTogMC44NXJlbTtcbiAgbWFyZ2luOiAtNXB4IDAgMDtcbn1cblxuLnNwaW5uZXItY29udGFpbmVyIHtcbiAgaGVpZ2h0OiAxMDB2aDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/user-list/user-list.page.ts":
/*!*********************************************!*\
  !*** ./src/app/user-list/user-list.page.ts ***!
  \*********************************************/
/*! exports provided: UserListPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserListPage", function() { return UserListPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm2015/store.js");
/* harmony import */ var _user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../user.service */ "./src/app/user.service.ts");
/* harmony import */ var _store_user_user_action__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../store/user/user.action */ "./src/store/user/user.action.ts");







let UserListPage = class UserListPage {
    constructor(userService, router, store) {
        this.userService = userService;
        this.router = router;
        this.store = store;
        this.userInfo = [];
        this.currentPagination = 0;
    }
    ngOnInit() {
        this.getMoreUsers();
        this.userService
            .getUserPicModificationNotifier()
            .subscribe(modification => {
            this.userInfo[modification.index].picture.large = this.userInfo[modification.index].picture.medium = modification.base64image;
        });
    }
    ionViewDidEnter() {
        if (this.virtualScroll) {
            this.virtualScroll.checkEnd();
        }
    }
    getMoreUsers(callback) {
        this.currentPagination++;
        this.userService.getUser(this.currentPagination).subscribe(users => {
            this.userInfo.push(...users);
            if (callback) {
                callback();
            }
        });
    }
    loadMoreUsersByScroll(event) {
        this.getMoreUsers(() => {
            event.target.complete();
            this.virtualScroll.checkEnd();
        });
    }
    goToUserDetail(user, index) {
        user.index = index;
        this.store.dispatch(Object(_store_user_user_action__WEBPACK_IMPORTED_MODULE_6__["assignUserToCheckDetails"])(user));
        this.router.navigate(['/user-detail']);
    }
};
UserListPage.ctorParameters = () => [
    { type: _user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _ngrx_store__WEBPACK_IMPORTED_MODULE_4__["Store"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonInfiniteScroll"], { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonInfiniteScroll"])
], UserListPage.prototype, "infiniteScroll", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonVirtualScroll"], { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonVirtualScroll"])
], UserListPage.prototype, "virtualScroll", void 0);
UserListPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-user-list',
        template: __webpack_require__(/*! raw-loader!./user-list.page.html */ "./node_modules/raw-loader/index.js!./src/app/user-list/user-list.page.html"),
        styles: [__webpack_require__(/*! ./user-list.page.scss */ "./src/app/user-list/user-list.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
        _ngrx_store__WEBPACK_IMPORTED_MODULE_4__["Store"]])
], UserListPage);



/***/ })

}]);
//# sourceMappingURL=user-list-user-list-module-es2015.js.map