(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["user-detail-user-detail-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/user-detail/user-detail.page.html":
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/user-detail/user-detail.page.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content *ngIf=\"(userDetail$ | async) as userDetails\" class=\"ion-padding\">\n  <ion-list>\n    <img [src]=\"pictureTaken || userDetails.picture.large\" />\n    <ion-item>\n      First Name: {{userDetails.name.first}}\n    </ion-item>\n    <ion-item>\n      Last name: {{userDetails.name.last}}\n    </ion-item>\n    <ion-item>\n      Email: {{userDetails.email}}\n    </ion-item>\n    <ion-item>\n      phone: {{userDetails.phone}}\n    </ion-item>\n  </ion-list>\n\n  <ion-row>\n    <ion-col size=\"6\">\n      <ion-button expand=\"full\" (click)=\"goBackToList()\">\n        Go back\n      </ion-button>\n    </ion-col>\n    <ion-col size=\"6\">\n      <ion-button expand=\"full\" (click)=\"takePicture(userDetails)\">\n        take picture\n      </ion-button>\n    </ion-col>\n  </ion-row>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/camera.service.ts":
/*!***********************************!*\
  !*** ./src/app/camera.service.ts ***!
  \***********************************/
/*! exports provided: CameraService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CameraService", function() { return CameraService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");



var CameraService = /** @class */ (function () {
    function CameraService(camera) {
        this.camera = camera;
        this.options = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true
        };
    }
    CameraService.prototype.takePicture = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var picture, error_1;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.camera.getPicture(this.options)];
                    case 1:
                        picture = _a.sent();
                        console.log(picture);
                        return [2 /*return*/, "data:image/jpeg;base64," + picture];
                    case 2:
                        error_1 = _a.sent();
                        throw Error('Camera not available');
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    CameraService.ctorParameters = function () { return [
        { type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_1__["Camera"] }
    ]; };
    CameraService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_1__["Camera"]])
    ], CameraService);
    return CameraService;
}());



/***/ }),

/***/ "./src/app/user-detail/user-detail-routing.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/user-detail/user-detail-routing.module.ts ***!
  \***********************************************************/
/*! exports provided: UserDetailPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserDetailPageRoutingModule", function() { return UserDetailPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _user_detail_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./user-detail.page */ "./src/app/user-detail/user-detail.page.ts");




var routes = [
    {
        path: '',
        component: _user_detail_page__WEBPACK_IMPORTED_MODULE_3__["UserDetailPage"]
    }
];
var UserDetailPageRoutingModule = /** @class */ (function () {
    function UserDetailPageRoutingModule() {
    }
    UserDetailPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], UserDetailPageRoutingModule);
    return UserDetailPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/user-detail/user-detail.module.ts":
/*!***************************************************!*\
  !*** ./src/app/user-detail/user-detail.module.ts ***!
  \***************************************************/
/*! exports provided: UserDetailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserDetailPageModule", function() { return UserDetailPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _user_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./user-detail-routing.module */ "./src/app/user-detail/user-detail-routing.module.ts");
/* harmony import */ var _user_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./user-detail.page */ "./src/app/user-detail/user-detail.page.ts");







var UserDetailPageModule = /** @class */ (function () {
    function UserDetailPageModule() {
    }
    UserDetailPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _user_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__["UserDetailPageRoutingModule"]
            ],
            declarations: [_user_detail_page__WEBPACK_IMPORTED_MODULE_6__["UserDetailPage"]]
        })
    ], UserDetailPageModule);
    return UserDetailPageModule;
}());



/***/ }),

/***/ "./src/app/user-detail/user-detail.page.scss":
/*!***************************************************!*\
  !*** ./src/app/user-detail/user-detail.page.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "img {\n  height: 200px;\n  width: 200px;\n  -o-object-fit: cover;\n     object-fit: cover;\n  border-radius: 50%;\n  display: block;\n  margin: auto;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9qb2huZnVlbnRlcy9Eb2N1bWVudHMvUHJvamVjdHMvaW9uaWMgL2x1aXMgcGFsYWNpb3Mvc3JjL2FwcC91c2VyLWRldGFpbC91c2VyLWRldGFpbC5wYWdlLnNjc3MiLCJzcmMvYXBwL3VzZXItZGV0YWlsL3VzZXItZGV0YWlsLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGFBQUE7RUFDQSxZQUFBO0VBQ0Esb0JBQUE7S0FBQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLFlBQUE7QUNDRiIsImZpbGUiOiJzcmMvYXBwL3VzZXItZGV0YWlsL3VzZXItZGV0YWlsLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImltZyB7XG4gIGhlaWdodDogMjAwcHg7XG4gIHdpZHRoOiAyMDBweDtcbiAgb2JqZWN0LWZpdDogY292ZXI7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgZGlzcGxheTogYmxvY2s7XG4gIG1hcmdpbjogYXV0bztcbn1cbiIsImltZyB7XG4gIGhlaWdodDogMjAwcHg7XG4gIHdpZHRoOiAyMDBweDtcbiAgb2JqZWN0LWZpdDogY292ZXI7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgZGlzcGxheTogYmxvY2s7XG4gIG1hcmdpbjogYXV0bztcbn0iXX0= */"

/***/ }),

/***/ "./src/app/user-detail/user-detail.page.ts":
/*!*************************************************!*\
  !*** ./src/app/user-detail/user-detail.page.ts ***!
  \*************************************************/
/*! exports provided: UserDetailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserDetailPage", function() { return UserDetailPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _camera_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../camera.service */ "./src/app/camera.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../user.service */ "./src/app/user.service.ts");






var UserDetailPage = /** @class */ (function () {
    function UserDetailPage(camera, store, userService, nav) {
        this.camera = camera;
        this.store = store;
        this.userService = userService;
        this.nav = nav;
    }
    UserDetailPage.prototype.ngOnInit = function () {
        this.userDetail$ = this.store.pipe(Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_2__["select"])('user'));
    };
    UserDetailPage.prototype.takePicture = function (user) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _a;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.camera.takePicture()];
                    case 1:
                        _a.pictureTaken = _b.sent();
                        this.userService.sendAnUserPictureModification({
                            index: user.index,
                            base64image: this.pictureTaken
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    UserDetailPage.prototype.goBackToList = function () {
        this.nav.back();
    };
    UserDetailPage.ctorParameters = function () { return [
        { type: _camera_service__WEBPACK_IMPORTED_MODULE_3__["CameraService"] },
        { type: _ngrx_store__WEBPACK_IMPORTED_MODULE_2__["Store"] },
        { type: _user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"] }
    ]; };
    UserDetailPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-user-detail',
            template: __webpack_require__(/*! raw-loader!./user-detail.page.html */ "./node_modules/raw-loader/index.js!./src/app/user-detail/user-detail.page.html"),
            styles: [__webpack_require__(/*! ./user-detail.page.scss */ "./src/app/user-detail/user-detail.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_camera_service__WEBPACK_IMPORTED_MODULE_3__["CameraService"],
            _ngrx_store__WEBPACK_IMPORTED_MODULE_2__["Store"],
            _user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"]])
    ], UserDetailPage);
    return UserDetailPage;
}());



/***/ })

}]);
//# sourceMappingURL=user-detail-user-detail-module-es5.js.map